import { Injectable } from '@nestjs/common';
// import { UserEntity } from './user.entity';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { from, Observable } from 'rxjs';
import { UserEntity } from 'src/entity/user.entity';
import { DataEntity } from 'src/entity/userdata.entity';
import { User } from 'src/enterfaces/user.model';
import { NewTableEntity } from 'src/entity/table.entity';

@Injectable()
export class DataService {
  constructor(
    @InjectRepository(DataEntity)
    private readonly userDataRepository: Repository<DataEntity>,
    @InjectRepository(NewTableEntity)
    private userNewDataRepository: Repository<NewTableEntity>,
  ) {}
  creatdate(user: User, userData: DataEntity): Observable<DataEntity> {
    // userData.author = user;
    return from(this.userDataRepository.save(userData));
  }

  findAllPosts(userdataId: any): Observable<DataEntity[]> {
    return from(this.userDataRepository.find(userdataId));
  }
  findAllnewPosts(userNewDataId: any): Observable<NewTableEntity[]> {
    return from(this.userNewDataRepository.find(userNewDataId));
  }

  findRoadmapById(roadMapId: any): Observable<DataEntity> {
    // return from(this.userDataRepository.findOne(roadMapId));
    return from(this.userDataRepository.findOne({ where: { id: roadMapId } }));
  }

  updatePost(UserData: DataEntity, id: number): Observable<UpdateResult> {
    const user: User = new UserEntity();
    user.id = id;
    return from(this.userDataRepository.update(id, UserData));
  }

  deleteRoadMap(id: number): Observable<DeleteResult> {
    // const apartmentTypeRepo: Repository<UserEntity> = getRepository(CartEntity);
    return from(this.userDataRepository.delete(id));

    // const roadMap: UserData = this.userDataRepository.findOne({
    //   id: id,
    // });

    // if (!roadMap)
    //   throw new HttpException(409, `Apartment Type not found for ${id}`);
    // this.userDataRepository.delete(id);
    // return roadMap;
  }
}
