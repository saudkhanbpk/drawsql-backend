import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { DataEntity } from './userdata.entity';
@Entity('newtask')
export class NewTableEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => DataEntity, (dataEntity) => dataEntity.tables)
  @JoinColumn({ name: 'dataId' })
  tabledata: DataEntity;

  @Column('text', { array: true, nullable: true })
  column: string[];
}
