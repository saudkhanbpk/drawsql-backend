import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { NewTableEntity } from './table.entity';
import { UserEntity } from './user.entity';

@Entity('data')
export class DataEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: '' })
  diagramname: string;

  @Column({ default: '' })
  access: string;

  @Column({ default: '' })
  databaseType: string;

  @CreateDateColumn()
  createdAt: Date;

  @ManyToOne(() => UserEntity, { eager: true, nullable: true })
  @JoinColumn({ name: 'userId' })
  author: UserEntity;

  @OneToMany(() => NewTableEntity, (newTaskEntity) => newTaskEntity.tabledata, {
    cascade: true,
    onDelete: 'CASCADE',
    eager: true,
    nullable: true,
  })
  tables: NewTableEntity[];
}
