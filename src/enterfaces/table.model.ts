import { User } from './user.model';

export interface UserNewTask {
  id?: number;
  author?: User;
  members?: string;
  deadline?: string;
  created?: string;
  updateBy?: string;
  comments?: string;
}
