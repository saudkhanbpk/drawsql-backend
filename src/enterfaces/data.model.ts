import { NewTableEntity } from 'src/entity/table.entity';
import { User } from './user.model';

export interface UserData {
  id?: number;
  title?: string;
  dbname?: string;
  dataType?: string;
  createdAt?: Date;
  author?: User;
  newtask?: NewTableEntity;
  tasks?: string;
}
