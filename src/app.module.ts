import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { MulterModule } from '@nestjs/platform-express';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthController } from './controllers/auth.controller';
import { DataController } from './controllers/userdata.controller';
import { NewTableEntity } from './entity/table.entity';
// import { DataController } from './controllers/data.controller';
// import { NewTaskEntity } from './entity/taske.entity';
import { UserEntity } from './entity/user.entity';
import { DataEntity } from './entity/userdata.entity';
import { AuthService } from './services/auth.service';
import { DataService } from './services/data.service';
// import { DataService } from './sevices/data.service';
// import { DataEntity } from './data.entity';
// import { NewTaskEntity } from './newData.entity';
// import { PositionEntity } from './position.entity';
// import { UserEntity } from './user.entity';

@Module({
  imports: [
    MulterModule.register({
      dest: './images',
    }),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'database-2.ckqlqhvbkztg.us-east-2.rds.amazonaws.com',
      port: 5432,
      username: 'postgres',
      password: '393939Sk',
      database: 'drawsql',
      entities: [UserEntity, DataEntity, NewTableEntity],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([UserEntity, DataEntity, NewTableEntity]),
    JwtModule.register({
      secret: 'secret',
      signOptions: { expiresIn: '1d' },
    }),
  ],
  controllers: [AppController, DataController, AuthController],
  providers: [AppService, DataService, AuthService],
})
export class AppModule {}
